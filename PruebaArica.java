//package HolaMundo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Scanner;

public class PruebaArica {
	public static Scanner sc = new Scanner(System.in); //como global porque o si no me queda la escoba :v //EDIT: Sigo con la escoba pero de momento se queda acÃ¡ :'v //EDIT2: Al final Roberto rehizo el menu xD
	//VALIDACIONES
	public static boolean EsNumero(String letra) {//Verifica que una cadena representa un numero
		boolean retorno;
		if(letra.equals("0")| letra.equals("1") | letra.equals("2") |  letra.equals("3") | letra.equals("4") | letra.equals("5") | letra.equals("6") | letra.equals("7") | letra.equals("8") | letra.equals("9"))
			retorno = true;
		else retorno = false;
		return retorno;
	}
	public static boolean EsNota(String nota) {//Verifica que una cadena representa una nota
		boolean retorno;
		int i;
		retorno = false;
		if(nota.length() == 2) {
			for (i=0; i < nota.length(); i++ ) {
				if (EsNumero(nota.substring(i,i+1))) {
					retorno = true;
				}
				if ( retorno == true && ((Integer.parseInt(nota)<10 || Integer.parseInt(nota)>70))) { //nota debe ser menor a 70 y mayor a 10
					retorno = false;
				}
			}
		}
		else
			retorno = false;
		return retorno;
	}
	public static boolean ValidarDia(String dia, int mes) { //Valida DÃ¯Â¿Â½a
		boolean b;
		int c,i,cont;
		String subdia,dianum;
		dianum ="";
		cont = 0;
		for (i=0; i<dia.length(); i++) {
			subdia = dia.substring(i,i+1);
			if (EsNumero(subdia)) {
				cont = cont +1;
				dianum=dianum+subdia;
			}
		}

		if( (cont==1 || cont==2) && dianum.length()==dia.length() ) {
			c = Integer.parseInt(dianum);
			if (mes==2){  //febrero
				if (c>0 & c<30){
					b= true;
				}
				else {
					System.out.println("El Dia debe ser un numero entre 1 y 29 (ambos incluidos)");
					b = false;
				}
			}
			else if (mes==1 || mes==3 || mes == 5 || mes==7 || mes==8 || mes == 10 || mes == 12){ //meses con 31 dias
				if ( c>0 & c<32) {
					b = true;
				} else {
					System.out.println("El Dia debe ser un numero entre 1 y 31 (ambos incluidos)");
					b = false;
				}
			}
			else{
				if ( c>0 & c<31) { //meses con 30 dias
					b = true;
				} else {
					System.out.println("El Dia debe ser un numero entre 1 y 30 (ambos incluidos)");
					b = false;
				}
			}
		} else {
			b = false;
		}
		return b;
	}
	public static boolean ValidarMes(String mes) {//valida mes
		boolean b;
		int c,i,cont;
		String submes,mesnum;
		mesnum ="";
		cont = 0;
		for (i=0; i<mes.length(); i++) {
			submes = mes.substring(i,i+1);
			if (EsNumero(submes) == true) {
				cont = cont +1;
				mesnum = mesnum + submes;
			}

		}
		if( (cont==1 || cont==2) && mesnum.length()==mes.length() ) {
			c = Integer.parseInt(mesnum);
			if ( c>0 & c<13 ) {
				b = true;
			} else {
				System.out.println("El Mes debe ser un numero entre 1 y 12 (ambos incluidos)");
				b = false;
			}
		} else {
			b = false;
		}

		return b;
	}
	public static boolean validarA(String a) { //valida aÃ¯Â¿Â½o
		boolean b;
		int c,i,cont;
		String subA,anoNum;
		cont = 0;
		anoNum ="";

		for (i=0; i<a.length(); i++) {
			subA = a.substring(i,i+1);
			if (EsNumero(subA)) {
				cont = cont +1;
				anoNum = anoNum + subA;
			}

		}

		if( cont==4 && anoNum.length()==a.length()) {
			c = Integer.parseInt(anoNum);
			if ( c>1900 & c<2030 ) {
				b = true;
			} else {
				System.out.println("El a\u00f1o debe ser un numero de cuatro cifras");
				b = false;
			}
		} else {
			b = false;
		}

		return b;
	}

	public static boolean ValidarFecha(int dia, int mes, int a) {
		boolean correcto = false;

		try {
			//Formato de fecha (dÃ­a/mes/aÃ±o)
			SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
			formatoFecha.setLenient(false);
			//ComprobaciÃ³n de la fecha
			formatoFecha.parse(dia + "/" + mes + "/" + a);
			correcto = true;
		} catch (ParseException e) {
			//Si la fecha no es correcta, pasarÃ¡ por aquÃ­
			correcto = false;
		}

		return correcto;
	}


	public static boolean EsOpValida(String ingreso, int min, int max){ //recibe el ingreso, verifica que sea un numero y que estÃ¯Â¿Â½ en el rango aceptable
		boolean retorno = false;
		if((ingreso.length()==1)&(EsNumero(ingreso))) {
			if((Integer.parseInt(ingreso)>=min) & (Integer.parseInt(ingreso)<=max)){
				retorno = true;
			}
		}
		if(retorno == false) {
			System.out.println("La opcion ingresada no es valida. Intente de nuevo.");
		}
		return retorno;
	}
	public static boolean EsFono(String ingreso) {
		boolean retorno = true;
		int i;

		if (ingreso.length()==9) {
			for (i=0; i<(ingreso.length()-1); i++) {
				if(EsNumero(ingreso.substring(i, i))) {
					retorno = false;
				}
			}
		}
		else retorno = false;

		return retorno;
	}
	public static boolean EsCorreo(String ingreso) {
		boolean retorno = false;
		int i, arrobas = 0, puntos = 0,posarrobas =1, posUltimoPunto= -1;

		for (i=0; i<ingreso.length(); i++) {
			if(ingreso.substring(i, i+1).equals(".")) {
				puntos = puntos + 1;
				posUltimoPunto = i;
			}
			if(ingreso.substring(i, i+1).equals("@")) {
				arrobas = arrobas + 1;
				posarrobas = i;
			}
		}

		if(arrobas ==1 && puntos>0 && posarrobas<(posUltimoPunto) && posUltimoPunto<(ingreso.length()) ) {
			retorno = true;
		}

		return retorno;
	}
	public static boolean EsVacio(String ingreso) {
		boolean retorno = false;
		if (ingreso.equalsIgnoreCase(""))
			retorno = true;
		return retorno;
	}

	/// Validaciones para el RUT
	public static boolean RutUnico(String[][] r, String rut) {
		int j = 0;
		boolean variableRetorno = true;
		while (j < 30) {
			if (rut.equals(r[j][1])) {
				variableRetorno = false;
				j = 30;
			}
			j++;
		}
		return variableRetorno;
	}
	
	public static boolean EsRut(String rut) { //Verifica si el rut ingresado es un rut valido

		String cuerpo[],dv;
		int rutnum,dvnum, sum, serie[];
		rutnum=0;
		dvnum=0;
		boolean correcto = true;
		serie = new int[6];
		serie[0] = 2;
		serie[1] = 3;
		serie[2] = 4;
		serie[3] = 5;
		serie[4] = 6;
		serie[5] = 7;
		try {
			cuerpo=rut.split("-");
			if (cuerpo[1].equals("0"))
				cuerpo[1]="11";
			if (cuerpo[1].equalsIgnoreCase("k"))
				cuerpo[1]="10";
			rutnum=Integer.parseInt(cuerpo[0]);
			dvnum=Integer.parseInt(cuerpo[1]);
			int j=0;
			sum=0;
			for (int i = cuerpo[0].length()-1; i>=0; i--) {
				sum=sum+Integer.parseInt(""+cuerpo[0].charAt(i))*serie[j];
				j++;
				if (j==6)
					j=0;
			}
			int resto=sum%11;
			resto=11-resto;
			if (resto!=dvnum)
				correcto=false;
		} catch (Exception e) {
			correcto=false;
		}
		return correcto;
	}

	// FIN VALIDACIONES


	public static String ImprimeMenu(String[][] r) {
		Scanner sc = new Scanner(System.in);
		Scanner scA = new Scanner(System.in);
		int control =0;
		String opMenu = "";
		String opAlu = "";
		String opNota = "";
		String opBus = "";


		while (!opMenu.equals("6")) {

			System.out.println("MENU PRINCIPAL");
			System.out.println("1. Opciones de Alumno");
			System.out.println("2. Opciones de Notas");
			System.out.println("3. Busqueda");
			System.out.println("4. Ver estado actual del curso");
			System.out.println("5. Reporte de Alumno");
			System.out.println("6. Salir");
			System.out.print("Su opcion: ");
			//opMenu = System.console().readLine();
			//System.out.print("\n");
			opMenu=sc.nextLine();

			if (opMenu.equals("1")) {
				//opciones de alumno

				while (!opAlu.equals("6")) {
					System.out.println("MENU ALUMNOS");
					System.out.println("1. Agregar Alumno");
					System.out.println("2. Actualizar Datos");
					System.out.println("3. Ver Nomina Actual");
					System.out.println("4. Ver Promedio Alumno");
					System.out.println("5. Eliminar Alumno");
					System.out.println("6. Atras");
					System.out.print("Su opcion: ");
					opAlu = sc.nextLine();
					//opAlu = System.console().readLine();


					if (opAlu.equals("1")) {
						ingresarAlumno(r);
						System.out.println("\n----------------------------------------------------------------");
						break;
					}
					else if (opAlu.equals("2")) {
						//actualizar datos
						actualizarAlumno(r);
						System.out.println("\n----------------------------------------------------------------");
						break;
					}
					else if (opAlu.equals("3")) {
						MuestraNomina(r,30);
						System.out.println("\n----------------------------------------------------------------");
						break;
					}
					else if (opAlu.equals("4")) {
						//ver estado actual del curso
						mostrarPromedio(r);
						System.out.println("\n----------------------------------------------------------------");
						break;

					}
					else if (opAlu.equals("5")) {
						//Eliminar alumno
						eliminarAlumno(r);
						System.out.println("\n----------------------------------------------------------------");
						break;

					}
					else if (opAlu.equals("6")) {
						System.out.println("volviendo a menu principal...");
						System.out.println("\n----------------------------------------------------------------");
						break;
					}
					else {
						System.out.println("opcion invalida");
					}
				}


			}
			else if (opMenu.equals("2")) {
				//opciones de notas
				while (!opNota.equals("4")) {
					System.out.println("MENU NOTAS");
					System.out.println("1. Ingresar o Modificar Notas");
					System.out.println("2. Notas del Curso");
					System.out.println("3. Eliminar Notas");
					System.out.println("4. Atras");
					System.out.print("Su opcion: ");
					opNota = sc.nextLine();

					if (opNota.equals("1")) {
						//ingresar notas

						ingresarNota(r);
						System.out.println("\n----------------------------------------------------------------");
						break;
					}
					else if (opNota.equals("2")) {
						//notas del curso
						control = MuestraNotas(r,30);
						System.out.println("\n----------------------------------------------------------------");
						break;
					}
					else if (opNota.equals("3")) {
						//eliminar notas
						eliminarNota(r);
						System.out.println("\n----------------------------------------------------------------");
						break;
					}
					else if (opNota.equals("4")) {
						System.out.println("volviendo a menu principal...");
						System.out.println("\n----------------------------------------------------------------");
						break;

					}
					else {
						System.out.println("opcion invalida");
					}


				}

			}

			else if (opMenu.equals("3")) {
				//busqueda
				while (!opBus.equals("3")) {
					System.out.println("MENU BUSQUEDAS");
					System.out.println("1. Buscar Alumno por RUT");
					System.out.println("2. Buscar Alumnos por nombre o apellido");
					System.out.println("3. Atras");
					System.out.print("Su opcion: ");

					opBus = sc.nextLine();



					if (opBus.equals("1")) {
						// busqueda por rut
						buscarPorId(r);
						System.out.println("\n----------------------------------------------------------------");

						break;
					} else if (opBus.equals("2")) {
						// busqueda por palabra
						buscarxPalabra(r);
						System.out.println("\n----------------------------------------------------------------");

					} else if (opBus.equals("3")) {
						System.out.println("volviendo a menu principal...");
						System.out.println("\n----------------------------------------------------------------");
						break;
					} else {
						System.out.println("opcion invalida");
					}
				}
			} else if (opMenu.equals("4")) {
				//ver estado actual del curso
				control = MuestraNotas(r,30);
				System.out.println("\n----------------------------------------------------------------");
			} else if (opMenu.equals("5")) {
				//reporte de alumno
				ReporteAlumno(r);
				System.out.println("\n----------------------------------------------------------------");

			} else if (opMenu.equals("6")) {
				return opMenu;
			} else {
				System.out.println("opcion invalida");
			}
		}
		return opMenu;
	}









	public static void ingresarAlumno (String[][] arr) { //Ingresa un nuevo alumno en memoria
		String rut, dv, cambio = "", rutComp;
		int cont = 0, i, x=0;
		//Scanner sc = new Scanner(System.in);
		for(i=0; i<30; i++) {
			if (arr[i][1] == "") {
				cont = cont + 1;
				x = i;
			}
		}
		if(cont > 0) {
			rutComp = "null";
			while(EsRut(rutComp)==false){
				System.out.println("Ingrese RUT del alumno sin digito verificador:");
				rut = sc.nextLine();
				System.out.println("Ingrese digito verificador:");
				dv = sc.nextLine();
				rutComp = rut.trim() +"-"+ dv.trim();
				if(EsRut(rutComp)==false) {
					System.out.println("Rut invalido. Intente nuevamente.");
				}
				if(!RutUnico(arr,rut)) {
					System.out.println("Rut ya existe. Intente nuevamente.");
					rutComp = "";
				} else {
					arr[x][1] = rut;
					arr[x][2] = dv;
				}
			}
			cambio = "";
			while(EsVacio(cambio)){
				System.out.println("Ingrese el nombre: ");
				cambio = sc.nextLine();
				arr[x][3] = cambio;
				if(EsVacio(cambio)) {
					System.out.println("Campo Obligatorio");
				}
			}
			cambio = "";
			while(EsVacio(cambio)){
				System.out.println("Ingrese el apellido paterno: ");
				cambio = sc.nextLine();
				arr[x][4] = cambio;
				if(EsVacio(cambio)) {
					System.out.println("Campo Obligatorio");
				}
			}
			cambio = "";
			while(EsVacio(cambio)){
				System.out.println("Ingrese el apellido materno: ");
				cambio = sc.nextLine();
				arr[x][5] = cambio;
				if(EsVacio(cambio)) {
					System.out.println("Campo Obligatorio");
				}
			}
			cambio = "";
			while(EsCorreo(cambio)==false){
				System.out.println("Ingrese el email: ");
				cambio = sc.nextLine();
				arr[x][6] = cambio;
				if(EsCorreo(cambio)==false) {
					System.out.println("Formato de Correo Invalido");
				}
			}
			cambio = "";
			while(EsFono(cambio)==false){
				System.out.println("Ingrese numero de contacto: ");
				cambio = sc.nextLine();
				arr[x][7] = cambio;
				if(EsFono(cambio)==false) {
					System.out.println("El telefono debe ser un valor numerico de 9 digitos");
				}
			}
			cambio = "";

			System.out.println("Ingrese fecha de nacimiento:");
			System.out.println("---------------------------");

				while(ValidarMes(cambio)==false){
					System.out.println("Ingrese mes: ");
					cambio = sc.nextLine();
					arr[x][9] = cambio;
					if(ValidarMes(cambio)==false) {
						//System.out.println("Ingrese un mes vÃ¯Â¿Â½lido");
					}
				}
				int mes = Integer.valueOf(arr[x][9]); ///// agrego el mes a una variable

				cambio = "";
				while(ValidarDia(cambio,mes)==false){ ///le paso el mes a la llamada de la funcion
					System.out.println("Ingrese dia: ");
					cambio = sc.nextLine();
					arr[x][8] = cambio;
					if(ValidarDia(cambio,mes)==false ) {
						//System.out.println("Ingrese un dÃ¯Â¿Â½a vÃ¯Â¿Â½lido.");
					}
				}
				cambio = "";
				while(validarA(cambio)==false){
					System.out.println("Ingrese a\u00f1o");
					cambio = sc.nextLine();
					arr[x][10] = cambio;
					if(validarA(cambio)==false) {
						//System.out.println("Ingrese un aÃ¯Â¿Â½o vÃ¯Â¿Â½lido");
					}
				}
			int dia = Integer.valueOf (arr[x][8]);
			int m = Integer.valueOf (arr[x][9]);
			int anio = Integer.valueOf (arr[x][10]);


			if (ValidarFecha(dia,m,anio)==true){

				System.out.println("la fecha esta correcta");
			}
			else {

				while (ValidarFecha(dia,m,anio)==false){
					System.out.println("la fecha esta incorrecta, re ingrese");
					while(ValidarMes(cambio)==false){
						System.out.println("Ingrese mes: ");
						cambio = sc.nextLine();
						arr[x][9] = cambio;
						if(ValidarMes(cambio)==false) {
							//System.out.println("Ingrese un mes vÃ¯Â¿Â½lido");
						}
					}
					int mes2 = Integer.valueOf(arr[x][9]); ///// agrego el mes a una variable

					cambio = "";
					while(ValidarDia(cambio,mes2)==false){ ///le paso el mes a la llamada de la funcion
						System.out.println("Ingrese dia: ");
						cambio = sc.nextLine();
						arr[x][8] = cambio;
						if(ValidarDia(cambio,mes2)==false ) {
							//System.out.println("Ingrese un dÃ¯Â¿Â½a vÃ¯Â¿Â½lido.");
						}
					}
					cambio = "";
					while(validarA(cambio)==false){
						System.out.println("Ingrese a\u00f1o");
						cambio = sc.nextLine();
						arr[x][10] = cambio;
						if(validarA(cambio)==false) {
							//System.out.println("Ingrese un aÃ¯Â¿Â½o vÃ¯Â¿Â½lido");
						}
					}
					dia = Integer.valueOf (arr[x][8]);
					m = Integer.valueOf (arr[x][9]);
					anio = Integer.valueOf (arr[x][10]);
				}
			}
			for(i=14; i<20; i++) {
				arr[x][i]="00";
			}
			System.out.println("\nAlumno ingresado Correctamente\n");
		} else {
			System.out.println("Cupos llenos. No se puede agregar otro alumno.\n");
		}

	}

	public static void actualizarAlumno (String[][] arr) { //Actualiza un alumno ya existente
		String rut,dv, cambio, opcion2;
		int i, j, opcion, x=0;
		boolean existe = false;
		//Scanner sc = new Scanner(System.in);
		System.out.println("Actualizacion Datos Alumno");
		System.out.println("Ingrese RUT del alumno sin digito verificador:");
		rut = sc.nextLine();
		System.out.println("Ingrese digito verificador:");
		dv = sc.nextLine();

		for(i=0; i<30; i++) {
			if ((arr[i][1].equals(rut) & (arr[i][2].equalsIgnoreCase(dv)))) {
				existe = true;
				x = i;
			}
		}

		if(existe == true) {
			System.out.println("----------Datos del Alumno-------------");
			System.out.println(arr[x][1]+ "-"+ arr[x][2]+ " | "+ arr[x][3]+" "+ arr[x][4]+" "+ arr[x][5]+ " | "+ arr[x][6]+ " | "+ arr[x][7]+ " | "+ arr[x][8]+"/"+arr[x][9]+"/"+arr[x][10]);
			System.out.println(" ");
			System.out.println("Que desea modificar?");
			System.out.println("1. Nombre");
			System.out.println("2. Primer apellido");
			System.out.println("3. Segundo apellido");
			System.out.println("4. Email");
			System.out.println("5. Numero de contacto");
			System.out.println("6. Fecha de Nacimiento");
			System.out.println("");
			System.out.println("0. Salir");

			opcion2 = sc.nextLine();
			while(EsOpValida(opcion2,0,6)==false){
				opcion2 = sc.nextLine();
			}
			opcion = Integer.parseInt(opcion2);
			cambio = "";

			switch(opcion) {
				case 1:
					while(EsVacio(cambio)){
						System.out.println("Ingrese nuevo nombre: ");
						cambio = sc.nextLine();
						arr[x][3] = cambio;
						if(EsVacio(cambio)) {
							System.out.println("Error. Intente Nuevamente.");
						}
					}
					break;
				case 2:
					while(EsVacio(cambio)){
						System.out.println("Ingrese primer apellido: ");
						cambio = sc.nextLine();
						arr[x][4] = cambio;
						if(EsVacio(cambio)) {
							System.out.println("Error. Intente Nuevamente.");
						}
					}
					break;
				case 3:
					while(EsVacio(cambio)){
						System.out.println("Ingrese segundo apellido: ");
						cambio = sc.nextLine();
						arr[x][5] = cambio;
						if(EsVacio(cambio)) {
							System.out.println("Error. Intente Nuevamente.");
						}
					}
					break;
				case 4:
					while(EsCorreo(cambio)==false){
						System.out.println("Ingrese email: ");
						cambio = sc.nextLine();
						arr[x][6] = cambio;
						if(EsCorreo(cambio)==false) {
							System.out.println("Correo invalido. Intente Nuevamente.");
						}
					}
					break;
				case 5:
					while(EsFono(cambio)==false){
						System.out.println("Ingrese numero de contacto: ");
						cambio = sc.nextLine();
						arr[x][7] = cambio;
						if(EsFono(cambio)==false) {
							System.out.println("El numero debe contener 9 digitos. Intente nuevamente.");
						}
					}
					break;
				case 6:
					System.out.println("Ingrese fecha de nacimiento:");
					System.out.println("---------------------------");

					while(ValidarMes(cambio)==false){
						System.out.println("Ingrese mes: ");
						cambio = sc.nextLine();
						arr[x][9] = cambio;
						if(ValidarMes(cambio)==false) {
							//System.out.println("Ingrese un mes vÃ¯Â¿Â½lido");
						}
					}
					int mes = Integer.valueOf(arr[x][9]); ///// agrego el mes a una variable

					cambio = "";
					while(ValidarDia(cambio,mes)==false){ ///le paso el mes a la llamada de la funcion
						System.out.println("Ingrese dia: ");
						cambio = sc.nextLine();
						arr[x][8] = cambio;
						if(ValidarDia(cambio,mes)==false ) {
							//System.out.println("Ingrese un dÃ¯Â¿Â½a vÃ¯Â¿Â½lido.");
						}
					}
					cambio = "";
					while(validarA(cambio)==false){
						System.out.println("Ingrese a\\u00f1o");
						cambio = sc.nextLine();
						arr[x][10] = cambio;
						if(validarA(cambio)==false) {
							//System.out.println("Ingrese un aÃ¯Â¿Â½o vÃ¯Â¿Â½lido");
						}
					}
					int dia = Integer.valueOf (arr[x][8]);
					int m = Integer.valueOf (arr[x][9]);
					int anio = Integer.valueOf (arr[x][10]);


					if (ValidarFecha(dia,m,anio)==true){

						System.out.println("la fecha esta correcta");
					}
					else {

						while (ValidarFecha(dia,m,anio)==false){
							System.out.println("la fecha es incorrecta, re ingrese");
							while(ValidarMes(cambio)==false){
								System.out.println("Ingrese mes: ");
								cambio = sc.nextLine();
								arr[x][9] = cambio;
								if(ValidarMes(cambio)==false) {
									//System.out.println("Ingrese un mes vÃ¯Â¿Â½lido");
								}
							}
							int mes2 = Integer.valueOf(arr[x][9]); ///// agrego el mes a una variable

							cambio = "";
							while(ValidarDia(cambio,mes2)==false){ ///le paso el mes a la llamada de la funcion
								System.out.println("Ingrese dia: ");
								cambio = sc.nextLine();
								arr[x][8] = cambio;
								if(ValidarDia(cambio,mes2)==false ) {
									//System.out.println("Ingrese un dÃ¯Â¿Â½a vÃ¯Â¿Â½lido.");
								}
							}
							cambio = "";
							while(validarA(cambio)==false){
								System.out.println("Ingrese a\u00f1o");
								cambio = sc.nextLine();
								arr[x][10] = cambio;
								if(validarA(cambio)==false) {
									//System.out.println("Ingrese un aÃ¯Â¿Â½o vÃ¯Â¿Â½lido");
								}
							}
							dia = Integer.valueOf (arr[x][8]);
							m = Integer.valueOf (arr[x][9]);
							anio = Integer.valueOf (arr[x][10]);
						}
					}
					break;


				case 0:
					existe = false;
				default:
					break;
			}
			if(existe == true) {
				System.out.println("----------Datos del Alumno-------------");
				System.out.println(arr[x][1]+ "-"+ arr[x][2]+ " | "+ arr[x][3]+" "+ arr[x][4]+" "+ arr[x][5]+ " | "+ arr[x][6]+ " | "+ arr[x][7]+ " | "+ arr[x][8]+"/"+arr[x][9]+"/"+arr[x][10]);
			}

		} else {
			System.out.println("No se ha encontrado un Alumno con ese Rut\n");
		}
	}

	public static void MuestraNomina(String[][] arreglo, int cantidadAlumnos) { //Muestra una nÃ¯Â¿Â½mina de alumnos con sus datos
		String text = "";
		int i, j, control, cupos = 0;

		System.out.println("Rut Nombre Apellido1 Apellido2 | Correo | Num Contacto");
		for(i=0; i<cantidadAlumnos; i++) {
			text = "";
			if (!arreglo[i][1].equals("")) {
				text = text + arreglo[i][1] + "-";
				text = text + arreglo[i][2] + " ";
				text = text + arreglo[i][3] + " ";
				text = text + arreglo[i][4] + " ";
				text = text + arreglo[i][5] + " | ";
				text = text + arreglo[i][6] + " | ";
				text = text + arreglo[i][7];
				System.out.println(text);
			} else {
				cupos = cupos + 1;
			}
		}
		System.out.println("\nQuedan "+cupos+" cupo(s) disponible(s) en el curso.\n");
	}

	public static void mostrarPromedio(String[][] r) { //Muestra el promedio del Alumno
		int p = 0,j=0,i,x=0,suma=0,n,cont=0,cont2=1;
		String rut,dv;
		boolean existe = false;
		//Scanner sc = new Scanner(System.in);

		System.out.println("Promedio Alumno");
		System.out.println("Ingrese RUT del alumno sin digito verificador:");
		rut = sc.nextLine();
		System.out.println("Ingrese digito verificador:");
		dv = sc.nextLine();

		for(i=0; i<30; i++) {
			if ((r[i][1].equals(rut)) && (r[i][2].equalsIgnoreCase(dv))) {
				existe = true;
				x = i;
			}
		}

		//ciclo para calcular sumatoria
		if(existe) {
			for(j=14; j<20; j++) {
				n = Integer.parseInt(r[x][j]);
				suma = suma + n;
				if(n != 0) {
					cont=cont+1;
				}
			}
		} else {
			System.out.println("No se ha encontrado un Alumno con ese Rut.");
			System.out.println("");
		}

		//calculo del promedio
		if (cont == 0) {
			System.out.println("No hay notas que mostrar.");
			System.out.println("");
		} else {
			p = Math.round(suma/cont);
			for(j=14; j<20; j++) {
				System.out.println("Nota "+cont2+": ["+NotaConComa(r[x][j])+"]");
				cont2=cont2+1;
			}
			System.out.println("");
			System.out.println("El promedio del alumno "+ r[x][3]+ " "+r[x][4]+" es: ["+ NotaConComa(p+"")+"]");
			System.out.println("");
		}

	}

	public static void eliminarAlumno (String[][] arreglo) { //Eliminar Alumno
		int i, x = 0;
		String rut,dv,ingreso;
		boolean existe = false;
		//Scanner sc = new Scanner(System.in);
		ingreso="";
		System.out.println("Eliminar Datos Alumno");
		System.out.println("Ingrese RUT del alumno sin digito verificador:");
		rut = sc.nextLine();
		System.out.println("Ingrese digito verificador:");
		dv = sc.nextLine();

		for(i=0; i<30-1; i++) {
			if ((arreglo[i][1].equals(rut)) && (arreglo[i][2].equalsIgnoreCase(dv))) {
				existe = true;
				x = i;
			}
		}

		if(existe) {
			System.out.printf("Eliminar al alumno %s %s cuyo rut es %s-%s? Ingrese 1 para confirmar o cualquier otra tecla para cancelar: ",arreglo[x][3],arreglo[x][4],rut,dv);
			ingreso=sc.nextLine();
			if (ingreso.equals("1")) {
				for(i=0; i<19; i++) {
					arreglo[x][i] = "";
					if(i > 13) {
						arreglo[x][i] = "00";
					}
				}
				System.out.println("El alumno ha sido eliminado exitosamente");
				System.out.println("");
			} else {
				System.out.println("Operacion cancelada por el usuario");
				System.out.println("");
			}

		} else {
			System.out.println("No se ha encontrado un Alumno con ese Rut. No se han hecho cambios.");
			System.out.println("");
		}
	}

	//ingresar nota
	public static void ingresarNota (String[][] r) {
		String rut,dv,nota,modifT;
		boolean existe = false, modif = false;
		int cont = 1,i,j,op = 0;
		Scanner sc = new Scanner(System.in);

		System.out.println("Ingrese RUT del alumno sin digito verificador:");
		rut = sc.nextLine();
		System.out.println("Ingrese digito verificador:");
		dv = sc.nextLine();

		for(i=0; i<30; i++) {

			if ((r[i][1].equals(rut)) && (r[i][2].equals(dv))) {
				existe = true;
				op = i;
			}
		}

		if(existe == true) {
			for(j=14; j<20; j++) {
				if (r[op][j].equals("") || r[op][j].equals("00")) {
					nota = "";
					while(EsNota(nota) == false) {
						System.out.println("Ingrese nota"+(j-13)+ ": ");
						nota = sc.nextLine();
						r[op][j] = nota;
						if (EsNota(nota) == false) {
							System.out.println("La nota ingresada no es valida. Ingrese ambos numeros (Ejemplo: 40)");
						}
					}
					cont=cont+1;
				} else {
					System.out.println("La nota"+(j-13)+ " existe y es esta: "+ r[op][j]);

					System.out.println("Desea modificarla? 1=si/0=no ");
					modifT = "";
					while(!modifT.equals("1") & !modifT.equals("0")) {
						modifT = sc.nextLine();
						if(!modifT.equals("1") & !modifT.equals("0")) {
							System.out.println("Opcion Invalida. Ingrese nuevamente");
						}
					}
					if(modifT.equals("0")){
						modif = false;
					} else if(modifT.equals("1")) {
						modif = true;
					}
					if (modif == true) {
						nota = "";
						while(EsNota(nota) == false) {
							System.out.println("ingrese nueva nota");
							nota = sc.nextLine();
							r[op][j] = nota;
							if (EsNota(nota) == false) {
								System.out.println("La nota ingresada no es valida. Ingrese ambos numeros (Ejemplo: 40)");
							}
						}
						System.out.println("dato modificado");
					}

				}
			}
		} else {
			System.out.println("El alumno no existe");
		}

	}


	//Mostrar Notas
	public static int MuestraNotas(String[][] arreglo, int cantidadAlumnos) {
		String text = "";
		int i, j, control = 1, promedio;

		System.out.println("Rut Apellido, Nombre : [nota1] [nota2] [nota3] [nota4] [nota5] [nota6] [promedio]");
		for(i=0; i<cantidadAlumnos; i++) {
			if(!arreglo[i][1].equals("")) {
				promedio = 0;
				text = "";
				text = text + arreglo[i][1] + "-";
				text = text + arreglo[i][2] + " ";
				text = text + arreglo[i][4] + ", ";
				text = text + arreglo[i][3] + ": ";
				for(j=14; j<20; j++) {
					text = text + "[" + NotaConComa(arreglo[i][j]) + "] ";
					promedio = promedio+Integer.parseInt(arreglo[i][j]);
				}
				promedio = Math.round(promedio/6);
				text = text  + "[" + NotaConComa(""+promedio) + "] ";
				System.out.println(text);
			}
		}
		System.out.println("");
		System.out.println("*LAS NOTAS EN BLANCO NO ESTAN INGRESADAS.");
		System.out.println("");
		return control;
	}


	//Eliminar Notas
	public static void eliminarNota(String[][] r) {
		String rut, dv;
		int i, j, x = 0, op = 0 ,n, cont = 0, suma = 0;
		boolean existe = false, on = true;
		Scanner sc = new Scanner(System.in);

		System.out.println("Eliminar Nota");
		System.out.println("Ingrese RUT del alumno sin digito verificador:");
		rut = sc.nextLine();
		System.out.println("Ingrese digito verificador:");
		dv = sc.nextLine();

		for(i=0; i<30; i++) {
			if((r[i][1].equals(rut)) & (r[i][2].equals(dv))) {
				existe = true;
				x = i;
			}
		}

		for(j=14; j<20; j++) {
			cont = cont + 1;
			System.out.println("Nota "+cont+": ["+r[x][j]+"]");
			suma = suma+Integer.parseInt(r[x][j]);
		}

		while((on==true) && (suma!=0)) {
			suma = 0;
			for(j=14; j<20; j++) {
				suma = suma+Integer.parseInt(r[x][j]); //calcula que los 6 datos no esten vacios
			}

			System.out.println("Que nota desea eliminar?");
			n = sc.nextInt();

			if((n>0) && (n<7)) {
				System.out.println(n+13);
				suma = suma+Integer.parseInt(r[x][n+13]);
				r[x][n+13] = "00";
				System.out.println("Nota Eliminada");
			}

			if(suma == 0) {
				on = false;
				suma = 0;
			} else {
				System.out.println("Desea eliminar otra nota? 0=no/1=si:");
				op = sc.nextInt();
				if(op == 0) on = false;
				else on = true;
			}
		}
	}


	public static void buscarPorId(String[][] arr) { //buscar alumno por id (rut)
		String rut, dv;
		int i, j, suma = 0;
		rut="";
		dv="";
		boolean existe = false;
		//Scanner sc = new Scanner(System.in);
		while (rut.equals("") || dv.equals("")) {
			System.out.println("Ingrese RUT del alumno sin digito verificador:");
			rut = sc.nextLine();
			System.out.println("Ingrese digito verificador:");
			dv = sc.nextLine();
			if (rut.equals("") || dv.equals(""))
				System.out.println("Por favor no ingrese valores vacios. Intente de nuevo");
		}
		for(i=0; i<30; i++) {
			for(j=0; j<20; j++) {
				if((arr[i][1].equals(rut)) && (arr[i][2].equalsIgnoreCase(dv))) {
					suma = (Integer.parseInt(arr[i][14])) + (Integer.parseInt(arr[i][15])) + (Integer.parseInt(arr[i][16])) + (Integer.parseInt(arr[i][17])) + (Integer.parseInt(arr[i][18])) + (Integer.parseInt(arr[i][19]));
					System.out.println( "-------DATOS DEL ALUMNO:-------");
					System.out.println( "RUT: "+ arr[i][1]+ "-"+ arr[i][2]+ " |Nombre: "+ arr[i][3]+" |Apellidos: "+ arr[i][4]+" "+ arr[i][5]);
					System.out.println( "-------NOTAS-------------------");
					System.out.println( "Nota 1: ["+ NotaConComa(arr[i][14])+"]");
					System.out.println( "Nota 2: ["+ NotaConComa(arr[i][15])+"]");
					System.out.println( "Nota 3: ["+ NotaConComa(arr[i][16])+"]");
					System.out.println( "Nota 4: ["+ NotaConComa(arr[i][17])+"]");
					System.out.println( "Nota 5: ["+ NotaConComa(arr[i][18])+"]");
					System.out.println( "Nota 6: ["+ NotaConComa(arr[i][19])+"]");
					System.out.println( "Promedio del alumno: ["+NotaConComa((suma/6)+"")+"]");
					existe = true;
					i = 30;
					j = 20;
				}
			}
		}
		if (!existe) {
			System.out.println("No se ha encontrado un alumno con ese rut");
		}
		System.out.println("");
	}
	public static void buscarxPalabra(String[][] arr) { //buscar datos de alumno por palabra
		String nombre, palabra, comparador;
		int i, j, k, contador = 0, longi;
		boolean resultado = false;
		Scanner sc = new Scanner(System.in);
		longi=0;
		palabra="";
		comparador="";
		while (longi==0) {
			System.out.print("Ingrese palabra a buscar: ");
			palabra = sc.nextLine();
			longi = palabra.length();
			if (longi==0)
				System.out.println("Por favor ingrese algo. Intente nuevamente.");
		}
		palabra=palabra.toLowerCase();
		for (i=0; i<30; i++) {
			comparador=arr[i][3]+" "+arr[i][4];
			comparador=comparador.toLowerCase();
			if (comparador.contains(palabra)) {
				if (!resultado)
					System.out.println("Se han encontrado los siguientes resultados:");
				System.out.println("RUT: "+ arr[i][1]+ "-"+ arr[i][2]+ " |Nombre: "+ arr[i][3]+" |Apellidos: "+ arr[i][4]+" "+ arr[i][5]);
				resultado=true;
			}
		}
		if (!resultado) {
			System.out.println("No se han encontrado resultados");
		}
		System.out.println("");
	}

	public static void ReporteAlumno(String[][] arr) {
		String rut, dv,nombre;
		int i, j, suma;
		boolean encontrado = false;
		//Scanner sc = new Scanner(System.in);
		nombre="";
		rut="";
		dv="";
		while (nombre.equals("") || rut.equals("") || dv.equals("")) {
			System.out.println("Ingrese nombre de quien Autoriza");
			nombre = sc.nextLine();
			System.out.println("Ingrese RUT del alumno sin digito verificador:");
			rut = sc.nextLine();
			System.out.println("Ingrese digito verificador:");
			dv = sc.nextLine();
			if (nombre.equals("") || rut.equals("") || dv.equals(""))
				System.out.println("No deje campos vacios. Intente de nuevo");
		}
		if (!RutUnico(arr,rut)) {
			for(i=0; i<30; i++){
				for(j=0; j<20; j++){
					if ((arr[i][1].equals(rut)) && (arr[i][2].equalsIgnoreCase(dv)) ) {
						encontrado = true;
						suma = (Integer.parseInt(arr[i][14])) + (Integer.parseInt(arr[i][15])) + (Integer.parseInt(arr[i][16])) + (Integer.parseInt(arr[i][17])) + (Integer.parseInt(arr[i][18])) + (Integer.parseInt(arr[i][19]));
						boolean faltanNotas = false;
						int k;
						for ( k=0; k<6; k++) {
							if (Integer.parseInt(arr[i][14+k]) == 0 ) {
								faltanNotas = true;
							}
						}
						System.out.println("-------DATOS DEL ALUMNO:-------");
						System.out.println("RUT: "+ arr[i][1]+ "-"+ arr[i][2]+ " |Nombre: "+ arr[i][3]+" |Apellidos: "+ arr[i][4]+" "+ arr[i][5]);
						System.out.println("-------NOTAS-------------------");
						System.out.println("Nota 1: ["+ NotaConComa(arr[i][14])+ "] "+ NotaAPalabra(arr[i][14]));
						System.out.println("Nota 2: ["+ NotaConComa(arr[i][15])+ "] "+ NotaAPalabra(arr[i][15]));
						System.out.println("Nota 3: ["+ NotaConComa(arr[i][16])+ "] "+ NotaAPalabra(arr[i][16]));
						System.out.println("Nota 4: ["+ NotaConComa(arr[i][17])+ "] "+ NotaAPalabra(arr[i][17]));
						System.out.println("Nota 5: ["+ NotaConComa(arr[i][18])+ "] "+ NotaAPalabra(arr[i][18]));
						System.out.println("Nota 6: ["+ NotaConComa(arr[i][19])+ "] "+ NotaAPalabra(arr[i][19]));
						System.out.println("-------------------------------");
						System.out.println((arr[i][3]).toUpperCase() + " TIENE UN PROMEDIO DE "+ NotaConComa((suma/6)+"") + " (" + NotaAPalabra((suma/6)+"") + ")" + " Y ESTA ");
						if (( Math.round(suma/6)>= 40) & (faltanNotas == false) ) {
							System.out.println("APROBADO");
						} else {
							System.out.println("REPROBADO");
						}
						System.out.println("ACREDITADO POR: "+ nombre.toUpperCase());
						i = 30;
						j = 20;
					}
				}
			}
		} 
		
		if (!encontrado) {
			System.out.println("No se ha encontrado el alumno buscado.");
			System.out.println();
		}




	}

	public static void main(String[] args) {
		String [][] arr = new String [30][20];
		int i, j, control, ingreso;
		String opmenu;
		Scanner scan = sc; //new Scanner(System.in);
		for(i=0; i<30; i++){
			for(j=0; j<14; j++){
				arr[i][j] = "";
				if(j > 13)
					arr[i][j] = "00";
			}
		}
		
		arr[0][19] = "60";
		arr[0][18] = "50";
		arr[0][17] = "60";
		arr[0][16] = "60";
		arr[0][15] = "45";
		arr[0][14] = "50";
		arr[0][1] = "10100100";
		arr[0][2] = "2";
		arr[0][3] = "Jorge";
		arr[0][4] = "Perez";
		arr[0][5] = "Perez";
		arr[0][6] = "email";
		arr[0][7] = "telefono";
		arr[0][8] = "04";
		arr[0][9] = "12";
		arr[0][10] = "1997";
		arr[1][19] = "50";
		arr[1][18] = "40";
		arr[1][17] = "70";
		arr[1][16] = "20";
		arr[1][15] = "39";
		arr[1][14] = "41";
		arr[1][1] = "16947638";
		arr[1][2] = "1";
		arr[1][3] = "Pedro";
		arr[1][4] = "Pedreros";
		arr[1][5] = "Pedroso";
		arr[1][6] = "email";
		arr[1][7] = "telefono";
		arr[1][8] = "04";
		arr[1][9] = "12";
		arr[1][10] = "1995";

		opmenu = "";
		while (!opmenu.equals("6")) {
			System.out.println("---------------------------------------------------------------");
			System.out.println("Bienvenido al sistema de gestion de alumnos y notas de Arica");
			System.out.println("Ingrese numero de opcion a la que desee ingresar y aprete Enter");
			System.out.println("---------------------------------------------------------------");
			System.out.println("Para comenzar presione Enter");
			scan.nextLine();
			opmenu=ImprimeMenu(arr);
		}


	}

	//MÃ©todos Adicionales Asociados a Notas
	public static String NotaAPalabra(String nota) {//convierte una nota a palabra
		String retorno, prep;
		int notaNum;
		notaNum = Math.round(Integer.parseInt(nota));
		prep = notaNum+"";
		if (notaNum<1)
			prep = "00";
		retorno = NumAPalabra(prep.substring(0,1))+" COMA ";
		retorno = retorno + NumAPalabra(prep.substring(1,2));
		if( retorno.equals("CERO COMA CERO") ){
			retorno = "NOTA FALTANTE";
		}
		return retorno;
	}


	public static String NotaConComa(String nota) { //Escribe la nota de decenas y unidades a unidad y coma. Si es 00, retorna vacio
		String retorno, prep;
		int notaNum;
		notaNum = Math.round(Integer.parseInt(nota));
		retorno = (notaNum/10)+"";

		if( notaNum%10 == 0 ) {
			retorno = retorno+".0";
		} else {
			retorno = retorno+"."+(notaNum%10);
		}
		if( retorno == "0.0" ) {
			retorno = "   ";
		}
		if((retorno.substring(0,1).equals("0")) && !(retorno.substring(2,3).equals("0"))) {
			retorno = "1.0";
		}
		return retorno;
	}


	public static String NumAPalabra(String nota) { //escribe un caracter de numero de una cifra a palabra (0-9)
		String retorno;
		int control = Integer.parseInt(nota);
		switch(control) {
			case 0: retorno = "CERO";
				break;
			case 1: retorno = "UNO";
				break;
			case 2: retorno = "DOS";
				break;
			case 3: retorno = "TRES";
				break;
			case 4: retorno = "CUATRO";
				break;
			case 5: retorno = "CINCO";
				break;
			case 6: retorno = "SEIS";
				break;
			case 7: retorno = "SIETE";
				break;
			case 8: retorno = "OCHO";
				break;
			case 9: retorno = "NUEVE";
				break;
			default: retorno = "";
		}
		return retorno;
	}

}
